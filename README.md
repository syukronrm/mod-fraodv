# MOD FRAODV

## Tujuan Modifikasi

Mengurangi routing overhead dengan mencatat _fair value_ dari _node neighbor_.

## Modifikasi

Modifikasi yang diusulkan pada protokol ini adalah:

1. Proses pertama sama seperti AODV

2. Pada proses propagasi RREQ, terdapat penghitungan FV pada header paket. Node yang menerima paket RREQ menambahkan informasi routing ke routing table, membandingkan FV, dan mengecek apakah node tersebut merupkan destination node. Jika bukan destination node dan belum menerima paket RREQ dengan RREQ ID yang sama, maka simpa FV yang terdapat pada RREQ di node, simpan node ID dan FV dari node di paket RREQ. Jika node sudah menerima RREQ dengan ID yang sama, maka bandingkan FV dari RREQ yang baru diterima dan FV yang terdapat pada node. Jika FV RREQ yang baru diterima lebih kecil dibandingkan yang terdapat pada node, maka replace FV yang sudah tersimpan pada routing table, jika tidak, drop paket RREQ.

3. FV ditambahkan pada paket RREP. RREP memiliki nilai dari semua nilai FV pada RREQ yang ditambahkan (summation). Jika node yang menerima RREQ dalam node destination, maka cek FV dan FV yang tersimpan di node. Jika FV dari RREQ yang baru diterima lebih kecil dibandingkan yang sudah ada, maka node mengirim RREQ paket dan mengirimkannya ke source node.

4. Source node memilih rute yang pertama diterima dari RREP. Jika node menerima RREP baru yang memiliki FV yang lebih kecil dibandingkan rute sekarang, maka node memiliki route yang baru diterima.

